# DS 01 : auto-correction

Bienvenue dans ce premier DS de l’année, au menu : calculs (opérations), commentaires, types de données, commande `print()`, variables, commande `input()`, fonctions, boucle for. Bon courage, c’est parti !

## A – Calculs _(2,5 pts)_

Python est une formidable calculatrice, saurez-vous trouver le résultat des calculs ci-dessous ?

Exemple :

```python
>>> 9 + 9 - 3

15
```

À vous de jouer ↓

```python
>>> 5 * (2 + 3) - (4 - (1 + 2))

24
```

Exercice suivant :

```python
>>> 14 // 4

3
```

Exercice suivant :

```python
>>> 14 % 4

2
```

Exercice suivant :

```python
>>> 6 ** 2

36
```

## B – Commentaires _(3 pts)_

Dans les codes ci-dessous, ajoutez vos **commentaires** pour décrire ce qu'est en train de faire la développeuse.

Exemple :

```python
#   
#   
for i in range(3):
    # 
    # 
    print("Python for ever")
```

Deviendra, une fois complété par vos soins :

```python
# lance une boucle qui va itérer trois fois  
# 
for i in range(3):
    # affiche dans la console : Python for ever
    # 
    print("Python for ever")
```

À vous de jouer ↓

```python
# on déclare une variable pokemon
# ayant pour valeur l’entier 100
# 
pokemon = 100
# 
# on déclare une variable monstre
# ayant la même valeur que celle stockée dans la variable pokemon
monstre = pokemon
# 
# on soustrait 50
# de la valeur stockée dans la variable monstre
monstre -= 50
```

Exercice suivant :

```python
# on demande la saisie de l'âge de l'utilisatrice / utilisateur
# en affichant la chaîne de caractères "quel est votre âge ? "
# et on stocke cette valeur dans une variable age que l'on déclare
age = input("quel est votre âge ? ")
# 
# affiche la valeur de la variable age
# suivie d’une chaîne de caractères
print(age, "ans ? Le temps passe vite !")
```

Exercice suivant :

```python
def addition(a, b):
    return a + b
# on exécute la fonction « addition » en renseignant deux paramètres
# et on stocke la valeur retournée par la fonction dans une nouvelle variable : c
c = addition(20, 3)
```

## C – Types de données _(2 pts)_

Pouvez-vous indiquer quel est le type de donnée utilisé ? Exemple :

```python
>>> 14 + 5

int
```

À vous de jouer ↓

```python
>>> 13.1

float
# ce nombre possède une virgule, on est donc en présence du type Python float
```

Exercice suivant :

```python
>>> """Arigatō"""

str
# cette donnée est entourée de guillemets, on est donc en présence du type Python str
```

Exercice suivant :

```python
>>> '8'

str
# cette donnée est entourée de guillemets, on est donc en présence du type Python str
```

Exercice suivant :

```python
>>> 8 / 4

float
# piège ! (toute la classe s'est trompée)
# en Python, une division, même de deux entiers, donne un float !
# ici, 8 divisé par 4 donne 2.0
# on est donc en présence du type Python float
```

## D – Print() _(1 pt)_

Savez-vous ce que Python affichera dans la console ?

Exemple :

```python
>>> print("Hello world")

Hello world
```

À vous de jouer ↓

```python
>>> age = 23
>>> age = age + 1
>>> print(age, "ans")

24 ans
```

Exercice suivant :

```python
>>> titre = "GTA"
>>> categorie = "jeu populaire"
>>> description = titre + " est un " + categorie + " ..."
>>> print(description)

GTA est un jeu populaire ...
```

## E – Code Puzzle _(2 pts)_

Quel mélange ! Pouvez-vous aider à remettre le code dans le bon sens ?

Exemple :

```python
solde = solde - depense
print("oops le solde est à zéro")
solde = 100
depense = 100
```

Deviendra :

```python
solde = 100
depense = 100
solde = solde - depense
print("oops le solde est à zéro")
```

À vous de jouer ↓

```python
print("b vaut 10")
b = b + 1
b = a ** 2
a = 3
```

Devient :

```python
a = 3
b = a ** 2
b = b + 1
print("b vaut 10")
```

Exercice suivant :

```python
c = c / 15
b = 20
c = a + b + 30
a = 100
print("c vaut 10.0")
```

Devient :

```python
a = 100
b = 20
c = a + b + 30
c = c / 15
print("c vaut 10.0")
```

## F – Valeur finale _(1,5 pts)_

À la fin de ces instructions, savez-vous quelle est la valeur finale de `x` ?

```python
a = 100
b = a
b -= 50
x = a + b
b -= 50

# à la fin, x vaut : 150
```

```python
# on assigne la valeur 0 à la variable x
x = 0
# puis on boucle (itère) 5 fois
# sur une variable i qui vaudra 0, 1, 2, 3, 4
for i in range(5):
    # on additionne la valeur stockée dans i
    # à la valeur stockée dans x
    x += i

# à la fin, x vaut : 10
```

Si on réalise le "traçage explicite" de l'exercice ci-dessus, cela donne :

| itération |  i  |  x  |
| :-------: | :-: | :-: |
|     1     |  0  |  0  |
|     2     |  1  |  1  |
|     3     |  2  |  3  |
|     4     |  3  |  6  |
|     5     |  4  | 10  |

Exercice suivant :

```python
# on assigne la valeur 0 à la variable x
x = 0
# puis on boucle (itère) 5 fois
# sur une variable i qui vaudra 0, 1, 2, 3, 4
for i in range(5):
    # et dans chaque itération de i
    # on boucle (itère) 4 fois
    # sur une variable j qui vaudra 0, 1, 2, 3
    for j in range(4):
        # on additionne la valeur 1
        # à la valeur stockée dans x
        x += 1

# à la fin, x vaut : 20
```

Si on réalise le "traçage explicite" de l'exercice ci-dessus, cela donne :

| itération (au total) |  i  |  j  |  x  |
| :------------------: | :-: | :-: | :-: |
|          1           |  0  |  0  |  1  |
|          2           |  0  |  1  |  2  |
|          3           |  0  |  2  |  3  |
|          4           |  0  |  3  |  4  |
|          5           |  1  |  0  |  5  |
|          6           |  1  |  1  |  6  |
|          7           |  1  |  2  |  7  |
|          8           |  1  |  3  |  8  |
|          9           |  2  |  0  |  9  |
|          10          |  2  |  1  | 10  |
|          11          |  2  |  2  | 11  |
|          12          |  2  |  3  | 12  |
|          13          |  3  |  0  | 13  |
|          14          |  3  |  1  | 14  |
|          15          |  3  |  2  | 15  |
|          16          |  3  |  3  | 16  |
|          17          |  4  |  0  | 17  |
|          18          |  4  |  1  | 18  |
|          19          |  4  |  2  | 19  |
|          20          |  4  |  3  | 20  |

## G – Code à trous _(3 pts)_

Un développeur distrait a oublié certaines lignes, pouvez-vous compléter son code ?

Exemple :

```python



bonjour() # affiche bonjour dans la console
```

Deviendra :

```python
def bonjour():
    print("bonjour")

bonjour() # affiche bonjour dans la console
```

À vous de jouer, complétez ces codes ↓

```python
def miaou():
    

print( miaou() ) # affiche miaou dans la console
```

Devient :

```python
# le développeur distrait a oublié
# dans sa fonction de retourner une valeur
def miaou():
    return "miaou"

print( miaou() ) # affiche miaou dans la console
```

Exercice suivant :

```python
def prix_ttc
    return prix_ht * 20 / 100 + prix_ht

prix_total = prix_ttc(100)
print("prix_total vaut 120.0")
```

Devient :

```python
# le développeur distrait a oublié
# de mentionner un paramètre nommé prix_ht
# et de suivre sa déclaration de fonction par
# le symbole "deux points"
def prix_ttc(prix_ht):
    return prix_ht * 20 / 100 + prix_ht

prix_total = prix_ttc(100)
print("prix_total vaut 120.0")
```

Exercice suivant :

```python
def perimetre(rayon):
    p = 2 * 3.14 * rayon


p1 = perimetre(25)
print("p1 vaut 157.0")
p2 = perimetre(50)
print("p2 vaut 314.0")
```

Devient :

```python
# le développeur distrait a oublié
# dans sa fonction de retourner une valeur
# (ici le résultat p du calcul du périmètre)
def perimetre(rayon):
    p = 2 * 3.14 * rayon
    return p

p1 = perimetre(25)
print("p1 vaut 157.0")
p2 = perimetre(50)
print("p2 vaut 314.0")
```

## H – Trou de mémoire _(2 pts)_

Votre ami Gabriel a un gros doute technique, pourriez-vous lui rappeler ce que fait la ligne suivante :

```python
play_all("Sali", "Sidibé")
```

À vous de lui expliquer ↓

```text
On appelle la fonction « play_all », en lui fournissant deux paramètres :
- La chaîne « Sali »  
- La chaîne « Sidibé »
```

## i – Étude de cas _(3 pts)_

« DJ Mayonnaise » sort toujours des albums constitués de quatre titres. Et comme il est très occupé à produire des titres, il vous a demandé de lui coder quelque chose qui puisse l'aider à générer ce genre de message :

```text
DJ Mayonnaise, nouvel album :
1. Tonton du bled remix
2. Changeling
3. Food truck
4. C'est pas la capitale
```

Que pourriez-vous lui proposer ?

```python
# On pourrait lui proposer une fonction
# qui lui permettrait de gagner un peu de temps
# lorsqu'il souhaite communiquer autour de la
# sortie d'un de ses nouvels albums

# on déclare une nouvelle fonction affiche_album
# qui prend quatres paramètres
def affiche_album(t1, t2, t3, t4):
    # on affiche un texte dans la console
    # en reprenant les valeurs stockées dans les paramètres
    print("DJ Mayonnaise, nouvel album :")
    print("1. " + t1)
    print("2. " + t2)
    print("3. " + t3)
    print("4. " + t4)

# on teste cette fonction
affiche_album("Post Reformat", "Easily Distracted", "May Days", "The Windham Song")

# ce qui afficherait ceci dans la console :
# DJ Mayonnaise, nouvel album :
# 1. Post Reformat
# 2. Easily Distracted
# 3. May Days
# 4. The Windham Song

# au fait, qui est DJ Mayonnaise ? Existe-t'il vraiment ?
# https://www.discogs.com/master/2871430-DJ-Mayonnaise-Still-Alive
```

---

Voilà c'est la fin du DS, merci 【ツ】

---

## J – Suppléments facultatifs

Les questions suivantes sont des suppléments _facultatifs_. Libre à vous de les faire, ou pas !

### Camille entre en seconde

Camille cherche une développeuse ou un développeur qui pourrait l'aider à coder une fonction qui lui permettrait de convertir n'importe quel nombre de secondes dans un format incluant à la fois jours, heures, minutes et secondes.

Avez-vous une proposition à faire à Camille ?

```python
# on déclare une nouvelle fonction
# que l'on pourrait appeler convert
# et qui prendrait un paramètre s
# correspondant aux nombres de secondes
# à convertir en jours, heures, minutes, secondes
def convert(s):
    # pour gagner en lisibilité dans notre fonction
    # on assigne les équivalences en secondes
    # pour une journée
    # pour une heure
    # pour une minute
    jour_s = 24 * 60 * 60
    heure_s = 60 * 60
    minute_s = 60
    # on calcule le nombre de jours
    # en conservant le nombre de secondes restant
    j = s // jour_s # // résultat entier d'une division
    s_restantes = s % jour_s # % reste d'une division
    # on calcule le nombre d'heures
    # en conservant le nombre de secondes restant
    h = s_restantes // heure_s
    s_restantes = s_restantes % heure_s
    # on calcule le nombre de minutes
    # en conservant le nombre de secondes restant
    m = s_restantes // minute_s
    s_restantes = s_restantes % minute_s
    # on affiche directement le résultat des conversions
    # dans la console
    print("jour(s) :", j)
    print("heure(s) :", h)
    print("minute(s) :", m)
    print("seconde(s) :", s_restantes)

# on teste la fonction
convert(100000)
# ce qui afficherait ceci dans la console :
# jour(s) : 1
# heure(s) : 3
# minute(s) : 46
# seconde(s) : 40


# DISCLAIMER :  
# ce code n’est évidemment pas le seul à pouvoir effectuer ce qui est demandé  
# d’autres façons de faire existent et existeront ...
```

### Oncle Georges

Oncle Georges offre toujours des cadeaux étranges. Cette année, il vous a offert un petit livre mystérieux, constitué de 255 pages numérotées de 1 à 255, en décimal. Le problème c'est que sur chaque page, il y a un « code » : un nombre à déchiffrer, qui a été écrit en binaire !

Extraits :

- Sur la page 1 il est écrit : _rdv page 11111111_
- Sur la page 255, il est écrit : _rdv page 1000000_
- Sur la page 128, il est écrit : _rdv page 11110010_

Pour pouvoir transformer ces nombres binaires en décimal plus rapidement, vous décidez de vous coder une nouvelle fonction Python **qui n'utilise aucune autre fonction Python**.

N'oubliez pas d'appeler cette fonction à la fin de votre proposition, pour montrer au monde entier comment l'utiliser ↓

```python
# on déclare une nouvelle fonction
# que l'on pourrait appeler bin_to_dec
# et qui prendrait huit paramètres
# correspondant aux huit bits
# à convertir en décimal
def bin_to_dec(a, b, c, d, e, f, g, h):
    # on en profite pour ajouter un docstring
    # qui documente notre nouvelle fonction
    # cf PEP 257 – Docstring Conventions
    # https://peps.python.org/pep-0257/
    """
    Prend chacune des huit valeurs d'un octet (8 bits)
    et retourne son équivalent, en décimal
    """
    # on assigne la valeur 0 à une variable r
    r = 0
    # on additionne à la valeur de r
    # 2 puissance 0 multiplié par
    # la valeur du premier bit
    r += 2**0 * h
    # on additionne à la valeur de r
    # 2 puissance 1 multiplié par
    # la valeur du deuxième bit
    r += 2**1 * g
    # idem pour les autres bits
    r += 2**2 * f
    r += 2**3 * e
    r += 2**4 * d
    r += 2**5 * c
    r += 2**6 * b
    r += 2**7 * a
    # on retourne la valeur de r
    return r

# on teste notre fonction
print( bin_to_dec(1, 1, 1, 1, 1, 1, 1, 1) ) # affiche 255
print( bin_to_dec(1, 0, 0, 0, 0, 0, 0, 0) ) # affiche 128
print( bin_to_dec(1, 1, 1, 1, 0, 0, 1, 0) ) # affiche 242

# DISCLAIMER :  
# ce code n’est évidemment pas le seul à pouvoir effectuer ce qui est demandé  
# d’autres façons de faire existent et existeront ...
```

---

Fin des suppléments 【ツ】

---
